import React from 'react'
import './App.css'

function App() {
  return (
    <div className="App">
      <header className="App-header">
        Pyramidion pop-up
      </header>
    </div>
  );
}

export default App
