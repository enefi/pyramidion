/* eslint-disable import/no-webpack-loader-syntax */

import $ from 'jquery';

import iconBulb from '!!raw-loader!./assets/bulb.svg';
import iconLol from '!!raw-loader!./assets/lol.svg';

console.log('Pyramidion started');

const cssPrefix = 'pyramidion-by-enefi--';
const mkCssClass = (x: string): string => `${cssPrefix}${x}`;

const ccVoteArrow = mkCssClass('VoteArrow');
const ccVoteArrowAnim = mkCssClass('VoteArrowAnim');

const styles = `
.${ccVoteArrow} {
  border: 0 !important;
}

.${ccVoteArrow} .score {
  top: 0;
}

.${ccVoteArrow} svg {
  width: 100%;
}

.${ccVoteArrow}.upmod svg,
.${ccVoteArrow}.downmod svg
{
  border-radius: 0.3em 0.3em 0 0;
  background: #333;
  transition: background 0.2s, filter 0.2s;
}

.${ccVoteArrow}.up svg #glass {
  fill-opacity: 0 !important;
}

.${ccVoteArrow}.up svg #shine > * {
  stroke-opacity: 0 !important;
}

.${ccVoteArrow}.upmod svg #glass {
  fill-opacity: 1 !important;
}

.${ccVoteArrow}.upmod svg #shine *,
.${ccVoteArrow}.upmod svg #glass
{
  stroke: white !important;
  stroke-opacity: 1 !important;
}

.${ccVoteArrow}.upmod svg #socket {
  fill: #999 !important;
}

.${ccVoteArrow}.upmod svg * {
  transition: fill-opacity 1s, stroke .2s;
}

.${ccVoteArrow}.upmod svg #shine * {
  transition: stroke-opacity 1.5s;
}

@keyframes pyramidionLol {
  0% {
    transform: translate(0, 0) scaleX(1) scaleY(1) skewY(0) rotate(0);
  }
  20% {
    transform: translate(0.4px, -0.4px) scaleX(0.95) scaleY(0.85) skewY(0deg) rotate(2deg);
  }
  50% {
    transform: translate(0, 0) scaleX(1) scaleY(1) skewY(0) rotate(0);
  }
  70% {
    transform: translate(0.4px, -0.35px) scaleX(0.95) scaleY(0.85) skewY(0deg) rotate(2deg);
  }
  100 % {
    transform: translate(0, 0) scaleX(1) scaleY(1) skewY(0) rotate(0);
  }
}

.${ccVoteArrow}.downmod.${ccVoteArrowAnim} svg #mouth,
.${ccVoteArrow}.downmod.${ccVoteArrowAnim} svg #leftEye,
.${ccVoteArrow}.downmod.${ccVoteArrowAnim} svg #rightEye
 {
  animation-name: pyramidionLol;
  animation-duration: 1s;
  animation-delay: 0s;
  animation-iteration-count: 1;
  animation-timing-function: ease-out;
  display: inline-block;
}

.${ccVoteArrow}.down svg {
  filter: grayscale(1);
}

.${ccVoteArrow}.downmod svg {
  filter: grayscale(0);
}
`;

export const setClass = (el: JQuery, cls: string, value: boolean) => {
  if (value) { el.addClass(cls); } else { el.removeClass(cls); }
  return el;
};

const main = () => {
  $('head').append($('<style>').text(styles));
  $('.arrow.up, .arrow.upmod').addClass(ccVoteArrow).prepend($(iconBulb)).css({backgroundImage: 'none'});
  const funnyArrow = $('.arrow.down, .arrow.downmod');
  const handleFunClick = (event: JQuery.ClickEvent) => {
    const el = $(event.currentTarget);
    setTimeout(() => setClass(el, ccVoteArrowAnim, !el.hasClass('down'))); // wait for orig. handler to change class
  };
  funnyArrow
    .addClass(ccVoteArrow)
    .on('click', handleFunClick)
    .prepend($(iconLol))
    .css({backgroundImage: 'none'})
  ;
  // TODO: fix voting in comments
  // TODO: reapplying after comment edit
};

main();

export {};
