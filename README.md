# Pyramidion

Tweaks for [SaidIt](https://saidit.net).

Project status: alpha

# Requirements

* [node](https://nodejs.org/) (see `.node-version`)
* [yarn](https://yarnpkg.com/getting-started/install)

# Build

## 0) Get source files

```sh
$ git clone https://gitlab.com/enefi/pyramidion.git
$ cd pyramidion
```

## 1) Install dependencies
```sh
$ yarn
```

## 2) Build browser extension
```
$ yarn build
```

## 3) Install browser extension

* go to extensions settings
* enable "Developer mode" 
* click "Load unpacked"
* select `build` directory (in a directory where you cloned Pyramidion to)

# Development

```sh
$ yarn watch
```

Add the extension to your browser same way as mention in section Build.

After making a change you have to reload extension in browser by clicking on ⟳ icon in the extensions list, or use an extension which can do that for you (e.g. [Extensions Reloader](https://chrome.google.com/webstore/detail/extensions-reloader/fimgfedafeadlieiabdeeaodndnlbhid)). Then you can refresh a tab and see changes.

# License

Source code is under [AGPLv3](LICENSE_AGPL3.md).  
Custom images (icons and so on) are under [CC0](LICENSE_CC0.md).

If you use something from here, it would be great if you could mention me in credits and/or send me a link to your project ☺ (but you are not required to do so - see the licenses above).
